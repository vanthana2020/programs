package com.atm;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;
import javax.swing.JScrollPane;
import java.awt.Font;

public class AdminPage extends JFrame implements ActionListener{
	String MySQLURL = "jdbc:mysql://localhost:3306/atm";
    String databseUserName = "root";
    String databasePassword = "vanthana@123";
    Connection con = null;
   
    Statement st,st1,st2;
    
	private JTextField txtname;
	private JTextField txtmobile;
	private JTextField txtacno;
	private JTextField txtdebit;
	private JTextField txtpin;
	private JTextField txtamt;
	private JTable tbacc;
	private JButton btnadd;
	private JButton btnrem;
	private JTextArea txtaddress;
	private JButton btnupdate;
	int cust_id=0;
	private JButton btnview ;
	private JTable tbcust;
	private JButton btnexit;
	
	public AdminPage() {
		getContentPane().setBackground(new Color(176, 196, 222));
		 
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cust name");
		lblNewLabel.setBounds(65, 71, 71, 21);
		getContentPane().add(lblNewLabel);
		
		txtname = new JTextField();
		txtname.setBounds(161, 71, 86, 20);
		getContentPane().add(txtname);
		txtname.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Address");
		lblNewLabel_1.setBounds(65, 122, 46, 14);
		getContentPane().add(lblNewLabel_1);
		
		 txtaddress = new JTextArea();
		txtaddress.setBounds(161, 117, 141, 37);
		getContentPane().add(txtaddress);
		
		JLabel lblNewLabel_2 = new JLabel("Mobile");
		lblNewLabel_2.setBounds(65, 174, 46, 14);
		getContentPane().add(lblNewLabel_2);
		
		txtmobile = new JTextField();
		txtmobile.setBounds(161, 171, 86, 20);
		getContentPane().add(txtmobile);
		txtmobile.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Acc no");
		lblNewLabel_3.setBounds(308, 74, 46, 14);
		getContentPane().add(lblNewLabel_3);
		
		txtacno = new JTextField();
		txtacno.setBounds(413, 71, 86, 20);
		getContentPane().add(txtacno);
		txtacno.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Debit cd_no");
		lblNewLabel_4.setBounds(308, 122, 71, 14);
		getContentPane().add(lblNewLabel_4);
		
		txtdebit = new JTextField();
		txtdebit.setBounds(413, 119, 86, 20);
		getContentPane().add(txtdebit);
		txtdebit.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Pin");
		lblNewLabel_5.setBounds(308, 174, 46, 14);
		getContentPane().add(lblNewLabel_5);
		
		txtpin = new JTextField();
		txtpin.setBounds(413, 171, 86, 20);
		getContentPane().add(txtpin);
		txtpin.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Amount");
		lblNewLabel_6.setBounds(308, 222, 46, 14);
		getContentPane().add(lblNewLabel_6);
		
		txtamt = new JTextField();
		txtamt.setBounds(413, 219, 86, 20);
		getContentPane().add(txtamt);
		txtamt.setColumns(10);
		
		 btnadd = new JButton("Add record");
		btnadd.setBounds(65, 256, 89, 23);
		getContentPane().add(btnadd);
		
		 btnrem = new JButton("Remove record");
		btnrem.setBounds(211, 256, 105, 21);
		getContentPane().add(btnrem);
		
		tbacc = new JTable();
		tbacc.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Account no", "cust_id", "Debitcd_no", "pin", "amount"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, Integer.class, Integer.class, Integer.class, Float.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tbacc.setBounds(382, 372, 363, 177);
		tbacc.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		getContentPane().add(tbacc);
		
		btnupdate = new JButton("UPDATE");
		btnupdate.setBounds(356, 256, 89, 23);
		getContentPane().add(btnupdate);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(611, 544, -29, -215);
		getContentPane().add(scrollPane);
		
		tbcust = new JTable();
		tbcust.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tbcust.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"cust_id", "name", "address", "mobile"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, Long.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tbcust.setBounds(10, 372, 344, 177);
		getContentPane().add(tbcust);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(45, 572, 663, -243);
		getContentPane().add(scrollPane_1);
		
		 btnview = new JButton("View");
		btnview.setBounds(487, 256, 89, 23);
		getContentPane().add(btnview);
		
		JLabel lblNewLabel_7 = new JLabel("cust_id");
		lblNewLabel_7.setBounds(10, 335, 46, 14);
		getContentPane().add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("name");
		lblNewLabel_8.setBounds(81, 335, 46, 14);
		getContentPane().add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("addr");
		lblNewLabel_9.setBounds(161, 335, 46, 14);
		getContentPane().add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("mobile");
		lblNewLabel_10.setBounds(243, 335, 46, 14);
		getContentPane().add(lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("cust_id");
		lblNewLabel_11.setBounds(399, 335, 46, 14);
		getContentPane().add(lblNewLabel_11);
		
		JLabel lblNewLabel_12 = new JLabel("Acno");
		lblNewLabel_12.setBounds(465, 335, 46, 14);
		getContentPane().add(lblNewLabel_12);
		
		JLabel lblNewLabel_13 = new JLabel("Deb cd no");
		lblNewLabel_13.setBounds(519, 335, 63, 14);
		getContentPane().add(lblNewLabel_13);
		
		JLabel lblNewLabel_14 = new JLabel("pin");
		lblNewLabel_14.setBounds(626, 335, 46, 14);
		getContentPane().add(lblNewLabel_14);
		
		JLabel lblNewLabel_15 = new JLabel("Amt");
		lblNewLabel_15.setBounds(678, 335, 46, 14);
		getContentPane().add(lblNewLabel_15);
		
		JLabel lblNewLabel_16 = new JLabel("ADMIN ");
		lblNewLabel_16.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_16.setBounds(270, 26, 51, 14);
		getContentPane().add(lblNewLabel_16);
		
		 btnexit = new JButton("CLOSE");
		btnexit.setBounds(291, 288, 89, 23);
		getContentPane().add(btnexit);
		
		
		addActionEvent();
	}
	public void fetch()
	{
		DefaultTableModel model=(DefaultTableModel)tbcust.getModel();
		String query="select * from customers;";
	
		 try {
	         con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
	         if (con != null) 
	            System.out.println("Database connection is successful !!!!");
	      	 st = con.createStatement();
	      	ResultSet rs  = st.executeQuery(query);
	      	tbcust.setModel(DbUtils.resultSetToTableModel(rs));
	      /*	while(rs.next())
	      	{
	      		
	      		int cust_id=rs.getInt("cust_id");
	      		String name=rs.getString("name");
	      		String address=rs.getString("address");
	      		long mobile=rs.getLong("mobile");
	      		model.addRow(
	      				new Object[] {cust_id,name,address,mobile});
	      	}*/
	      	rs.close();
		 }
		 catch (SQLException e) {
				// TODO Auto-generated catch block
			    	//JFrame frame=new JFrame("View Last transaction");
			    	//JOptionPane.showMessageDialog(frame,"Withdraw Amount exceeds minimum balance","Cant perform transaction",JOptionPane.ERROR_MESSAGE);
			    	
				e.printStackTrace();
			}
		 }
	
	public void fetch1()
	{
		DefaultTableModel model=(DefaultTableModel)tbacc.getModel();
		String query="select * from accounts;";
	
		 try {
	         con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
	         if (con != null) 
	            System.out.println("Database connection is successful !!!!");
	      	 st = con.createStatement();
	      	ResultSet rs  = st.executeQuery(query);
	      	tbacc.setModel(DbUtils.resultSetToTableModel(rs));
	      /*	while(rs.next())
	      	{
	      		int acno=rs.getInt("acno");
	      		int cust_id=rs.getInt("cust_id");
	      		int debitcd_no=rs.getInt("debitcd_no");
	      		int pin=rs.getInt("pin");
	      		int amount=rs.getInt("amount");
	      		model.addRow(
	      				new Object[] {acno,cust_id,debitcd_no,pin,amount});
	      	}*/
	      	rs.close();
		 }
		 catch (SQLException e) {
				// TODO Auto-generated catch block
			    	//JFrame frame=new JFrame("View Last transaction");
			    	//JOptionPane.showMessageDialog(frame,"Withdraw Amount exceeds minimum balance","Cant perform transaction",JOptionPane.ERROR_MESSAGE);
			    	
				e.printStackTrace();
			}
		
	}
	
	private void addActionEvent() {
		 btnadd.addActionListener(this);
		 btnrem.addActionListener(this);
		 btnupdate.addActionListener(this);
		 btnview.addActionListener(this);
		 btnexit.addActionListener(this);
		
	}
	
	public static void main(String[] args) {
		AdminPage frame=new AdminPage();
		 frame.setTitle("Admin page");
		 frame.setVisible(true);
		 frame.setBounds(50, 50, 600,600);
		
		
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()== btnadd) 
		 {
			String name=txtname.getText();
			String address=txtaddress.getText();
			Double mobile=Double.parseDouble(txtmobile.getText());
			Integer Accno=Integer.parseInt(txtacno.getText());
			Integer Debitno=Integer.parseInt(txtdebit.getText());
			Integer pin=Integer.parseInt(txtpin.getText());
			Float amt=Float.parseFloat(txtamt.getText());
			
			 try {
				 con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
				 
			String query1="insert into customers(name,address,mobile)values('"+name+"','"+address+"',"+mobile+");";
			String query2="insert into accounts(cust_id,acno,debitcd_no,pin,amount)values(last_insert_id(),"+Accno+","+Debitno+","+pin+","+amt+");";
		         if (con != null) 
		            System.out.println("Database connection is successful !!!!");
		      	 st1 = con.createStatement();	
		  
		      	 
		     	  int count1 =  st1.executeUpdate(query1);
		     	  System.out.println("Record modified.." + count1 );
		     	 st2 = con.createStatement();
		     	  int count2 =  st2.executeUpdate(query2);
		     	 System.out.println("Record modified.." + count1 );
		        	 if( count1==1 && count2==1)
		        	 {
		        		 JOptionPane.showMessageDialog(this,"Record inserted succesfully");
		        	 }
		        	 else

		        		 JOptionPane.showMessageDialog(this,"Record not inserted"); 
		 
			
		    } catch (SQLException e1) {
		    	e1.printStackTrace();
		}catch(NumberFormatException e1)
		{
			JOptionPane.showMessageDialog(this, "Enter values ");
		}
			 finally {
				 try {
					 con.close();
					st1.close();
					st2.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 }
	     
	}
		if (e.getSource()== btnrem) 
		 {
			//String Debitno=txtdebit.getText();
			String Accno=txtacno.getText();
			
			 try {
				 con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
				 String custid="select cust_id from accounts where acno="+Accno+";";
				
				 if (con != null) 
			            System.out.println("Database connection is successful !!!!");
				st2 = con.createStatement();
			
		      	ResultSet rs  = st2.executeQuery(custid);
		    	int cust=0;
		      	//tbacc.setModel(DbUtils.resultSetToTableModel(rs));
		      	if(rs.next())
		      	{
		      		
		      		cust=rs.getInt("cust_id");
		      		
		      	}
		      	
			System.out.println(cust);
			rs.close();
			String query1="delete from accounts where acno="+Accno+";";
			String query2="delete from customers where cust_id="+cust+";";
		         
		         if (con != null) 
		            System.out.println("Database connection is successful !!!!");
		      	 st1 = con.createStatement();	
		  
		      	 
		     	  int count1 =  st1.executeUpdate(query1);
		     	  System.out.println("Record modified.." + count1 );
		     	 int count2 =  st1.executeUpdate(query2);
		     	 System.out.println("Record modified.." + count2 );
		        	 if( count1==1 && count2==1 )
		        	 {
		        		 JOptionPane.showMessageDialog(this,"Record deleted succesfully");
		        	 }
		        	 else

		        		 JOptionPane.showMessageDialog(this,"Record not deleted"); 
		 
			
		    } catch (SQLException e1) {
		    	e1.printStackTrace();
		} 
			 catch(NumberFormatException e1)
				{
					JOptionPane.showMessageDialog(this, "Enter values ");
				}finally {
			 try {
				 con.close();
				st1.close();
				st2.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 }
		 }
		if (e.getSource()== btnupdate) 
		 {
			String name=txtname.getText();
			String address=txtaddress.getText();
			String mobile=txtmobile.getText();
			String Accno=txtacno.getText();
			String Debitno=txtdebit.getText();
			String pin=txtpin.getText();
			String amt=txtamt.getText();
			
			 try {
				 con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
				 String custid="select cust_id from accounts where acno="+Accno+";";
				
				 if (con != null) 
			            System.out.println("Database connection is successful !!!!");
				st2 = con.createStatement();
			
		      	ResultSet rs  = st2.executeQuery(custid);
		    	int cust=0;
		      	//tbacc.setModel(DbUtils.resultSetToTableModel(rs));
		      	if(rs.next())
		      	{
		      		
		      		cust=rs.getInt("cust_id");
		      		
		      	}
		      	
			System.out.println(cust);
			rs.close();
			String query1="update accounts set acno="+Accno+",debitcd_no="+Debitno+",amount="+amt+" where acno="+Accno+";";
			String query2="update customers set name='"+name+"',address='"+address+"',mobile="+mobile+" where cust_id="+cust+";";
			
			
		        
		         if (con != null) 
		            System.out.println("Database connection is successful !!!!");
		      	 st = con.createStatement();	
		         
		      	 
		     	  int count1 =  st.executeUpdate(query1);
		     	  System.out.println("Record modified.." + count1 );
		     	 st2 = con.createStatement();	
		     			  int count2 =  st2.executeUpdate(query2);
		     	 System.out.println("Record modified.." + count2 );

		        	 if( count1==1 && count2==1)
		        	 {
		        		 JOptionPane.showMessageDialog(this,"Record updated succesfully");
		        	 }
		        	 else

		        		 JOptionPane.showMessageDialog(this,"Record not updated"); 
		 
			 con.close();
		    } catch (SQLException e1) {
		    	e1.printStackTrace();
		}catch(NumberFormatException e1)
		{
			JOptionPane.showMessageDialog(this, "Enter values ");
		}
			 finally {
				 try {
					 con.close();
					st.close();
					st2.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 }
		 }
		if (e.getSource()== btnview) 
		 {
		fetch();
		fetch1();
		 }
		if (e.getSource()== btnexit) 
		 {
			 JOptionPane.showMessageDialog(this,"Thank you"); 
			 this.dispose();
		 }
		
}
}
