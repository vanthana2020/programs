package com.atm;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Color;

public class Chngpin extends JFrame implements ActionListener{
	private JTextField txtpin;
	private JButton btchngpin;
	public Account a1;
	public Chngpin(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("CHANGE PIN");
		lblNewLabel.setBackground(new Color(255, 255, 224));
		lblNewLabel.setBounds(166, 37, 150, 27);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("ENTER NEW PIN");
		lblNewLabel_1.setBounds(47, 93, 119, 27);
		getContentPane().add(lblNewLabel_1);
		
		txtpin = new JTextField();
		txtpin.setBounds(246, 96, 119, 24);
		getContentPane().add(txtpin);
		txtpin.setColumns(10);
		
		btchngpin = new JButton("CHANGE PIN");
		btchngpin.setBounds(149, 166, 126, 27);
		getContentPane().add(btchngpin);
		this.a1=a1;
		addActionEvent();
	}
	private void addActionEvent() {
		 btchngpin.addActionListener(this);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()== btchngpin) 
		 {
			int pin=Integer.parseInt(txtpin.getText());
		 UpdatePin d=new UpdatePin();
		if( d.chngpin(a1,pin))
		{
			 JOptionPane.showMessageDialog(this, "Pin Changed succesfully!!");
			 Login mframe=new Login(a1);
			 mframe.setVisible(true);
		        mframe.setBounds(50, 50, 600,600);
			    this.dispose();
		}
		else
		{
			JOptionPane.showMessageDialog(this, "Pin unChanged");
		 MenuFrame mframe=new MenuFrame(a1);
		 mframe.setVisible(true);
	        mframe.setBounds(50, 50, 600,600);
		     this.dispose();
		}
	      
		 }
		
	}
}
