package com.atm;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;

public class MenuFrame extends JFrame implements ActionListener {
	static MenuFrame mframe;
	public JButton btdeposit;
	public JButton btpinchg;
	public JButton btwithdraw;
	public JButton btview;
	public JButton btntransfer;
	public Account a1;
	private JButton btnexit;
	private JLabel lblwelcome;
	public MenuFrame(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		
		getContentPane().setLayout(null);
		
		btdeposit = new JButton("DEPOSIT");
		btdeposit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		btdeposit.setBackground(new Color(255, 255, 224));
		btdeposit.setBounds(44, 80, 158, 37);
		getContentPane().add(btdeposit);
		
		 btpinchg = new JButton("CHANGE PIN");
		 btpinchg.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		 btpinchg.setBackground(new Color(255, 255, 224));
		btpinchg.setBounds(44, 162, 158, 37);
		getContentPane().add(btpinchg);
		
		 btwithdraw = new JButton("WITHDRAW");
		 btwithdraw.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		 btwithdraw.setBackground(new Color(255, 255, 224));
		btwithdraw.setBounds(264, 80, 173, 37);
		getContentPane().add(btwithdraw);
		
		 btview = new JButton(" LAST TRANSACTIOSNS");
		 btview.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		 btview.setBackground(new Color(255, 255, 224));
		 btview.setBounds(264, 162, 173, 37);
		getContentPane().add(btview);
		
		 btntransfer = new JButton("TRANSFER AMOUNT");
		 btntransfer.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		btntransfer.setBackground(new Color(255, 255, 224));
		btntransfer.setBounds(44, 234, 158, 37);
		getContentPane().add(btntransfer);
		 btnexit = new JButton("EXIT");
		 btnexit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		 btnexit.setBackground(new Color(255, 255, 224));
		btnexit.setBounds(264, 234, 173, 37);
		getContentPane().add(btnexit);
		String welcome="Welcome "+a1.getName()+"\nYour accno"+a1.getAcno();
		lblwelcome = new JLabel(welcome);
		lblwelcome.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblwelcome.setBounds(56, 11, 381, 48);
		getContentPane().add(lblwelcome);
		this.a1=a1;
		 addActionEvent();
	}

	private void addActionEvent() {
		 btdeposit.addActionListener(this);
		 btwithdraw.addActionListener(this);
		 btpinchg.addActionListener(this);
		 btview.addActionListener(this);
		 btntransfer.addActionListener(this);
		 btnexit.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()== btdeposit) 
		 {
			
		 DepositFrame d=new DepositFrame(a1);
		 d.setVisible(true);
	     d.setBounds(50, 50, 600,600);
	      this.dispose();
		 }
		if (e.getSource()== btwithdraw) 
		 {
			
		WithdrawFrame w=new WithdrawFrame(a1);
		w.setVisible(true);
	     w.setBounds(50, 50, 600,600);
	     w.setTitle("Withdraw");
	      this.dispose();
		 }
		if (e.getSource()== btview) 
		 {
			
		 ViewTrans v=new ViewTrans(a1);
		 v.setVisible(true);
	     v.setBounds(50, 50, 600,600);
	     v.setTitle("ViewTransaction");
	     v.fetch(a1);
	      this.dispose();
		 }
		if (e.getSource()== btpinchg) 
		 {
			
		 Chngpin v=new Chngpin(a1);
		 v.setVisible(true);
		 v.setTitle("Change pin");
	     v.setBounds(50, 50, 600,600);
	    
	      this.dispose();
		 }
		if (e.getSource()== btntransfer) 
		 {
			
		 TransacFrame t=new TransacFrame(a1);
		 t.setTitle("Transfer");
		 t.setVisible(true);
	     t.setBounds(50, 50, 600,600);
	     
	      this.dispose();
		 }
		if (e.getSource()== btnexit) 
		 {
		
			JOptionPane.showMessageDialog(this,"THANK YOU");
	      this.dispose();
	      dispose();
	      getFrames();
	      
		 }
	}
}
