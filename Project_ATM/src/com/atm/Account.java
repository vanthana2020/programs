package com.atm;

public class Account {
private int acno, custid, debitcardno, pin;
private double balance, amt;
private String name,transtype;
public Account() {

	super();
	
	
}
public int getAcno() {
	return acno;
}
public void setAcno(int acno) {
	this.acno = acno;
}
public int getCustid() {
	return custid;
}
public void setCustid(int custid) {
	this.custid = custid;
}
public int getDebitcardno() {
	return debitcardno;
}
public void setDebitcardno(int debitcardno) {
	this.debitcardno = debitcardno;
}
public int getPin() {
	return pin;
}
public void setPin(int pin) {
	this.pin = pin;
}
public double getBalance() {
	return balance;
}
public void setBalance(double balance) {
	this.balance = balance;
}
public double getAmt() {
	return amt;
}
public void setAmt(double amt) {
	this.amt = amt;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getTranstype() {
	return transtype;
}
public void setTranstype(String transtype) {
	this.transtype = transtype;
}

}
