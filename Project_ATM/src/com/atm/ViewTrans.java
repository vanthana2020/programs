package com.atm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewTrans extends JFrame implements ActionListener{
	 String MySQLURL = "jdbc:mysql://localhost:3306/atm";
     String databseUserName = "root";
     String databasePassword = "vanthana@123";
     Connection con = null;Statement st,st1;
     public JButton btnback;
     public Account a1;
private JTable restable;
	@SuppressWarnings("serial")
	public ViewTrans(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		JLabel lblview = new JLabel("VIEW LAST  5 TRANSACTIONS");
		lblview.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblview.setBounds(249, 21, 183, 23);
		getContentPane().add(lblview);
		
		restable = new JTable();
		restable.setBackground(Color.WHITE);
		restable.setFont(new Font("Times New Roman", Font.BOLD, 13));
		restable.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(0, 0, 128), null, null, null));
		restable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Account no", "Trans_type", "Trans_date", "Amount"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, Float.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		restable.getColumnModel().getColumn(0).setMinWidth(20);
		restable.getColumnModel().getColumn(1).setPreferredWidth(79);
		restable.getColumnModel().getColumn(1).setMinWidth(29);
		restable.setBounds(36, 92, 575, 172);
		getContentPane().add(restable);
		
		 btnback = new JButton("BACK");
		 btnback.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnback.setBounds(279, 280, 89, 23);
		getContentPane().add(btnback);
		
		JLabel lblaccno = new JLabel("Acc no");
		lblaccno.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblaccno.setBounds(60, 67, 73, 14);
		getContentPane().add(lblaccno);
		
		JLabel lblNewLabel = new JLabel("Trans type");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(182, 67, 82, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Trans date");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_1.setBounds(359, 67, 73, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Amount");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setBounds(513, 67, 73, 14);
		getContentPane().add(lblNewLabel_2);
		this.a1=a1;
		addActionEvent();
	}
	public void addActionEvent() {
		
		 btnback.addActionListener(this); 
		  
	    }
	public void fetch(Account a1)
	{
		String query="select * from transactions where acno= "+a1.getAcno()+" order by trans_date desc limit 10;";
	
		 try {
	         con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
	         if (con != null) 
	            System.out.println("Database connection is successful !!!!");
	      	 st = con.createStatement();
	      	ResultSet rs  = st.executeQuery(query);
	      	restable.setModel(DbUtils.resultSetToTableModel(rs));
	      	
		 }
		 catch (SQLException e) {
				// TODO Auto-generated catch block
			    	//JFrame frame=new JFrame("View Last transaction");
			    	//JOptionPane.showMessageDialog(frame,"Withdraw Amount exceeds minimum balance","Cant perform transaction",JOptionPane.ERROR_MESSAGE);
			    	
				e.printStackTrace();
			}
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()== btnback) 
		 {
			
		 MenuFrame mframe=new MenuFrame(a1);
			 mframe.setVisible(true);
		        mframe.setBounds(50, 50, 700,600);
			this.dispose();
		 
		 }
	
	}
}
