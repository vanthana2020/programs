package com.atm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

public class MainLogin extends JFrame implements ActionListener {
	private JButton btnadmin;
	private JButton btnuser ;
	public static Account a1;
	public MainLogin() {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		 btnadmin = new JButton("ADMIN LOGIN");
		btnadmin.setBounds(168, 87, 127, 23);
		getContentPane().add(btnadmin);
		
		btnuser = new JButton("USER LOGIN");
		btnuser.setBounds(166, 135, 129, 23);
		getContentPane().add(btnuser);
		
		JLabel lblNewLabel = new JLabel("ATM INTERFACE");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(166, 25, 140, 32);
		getContentPane().add(lblNewLabel);
		addActionEvent();
		
	}
	public void addActionEvent() {
		 btnuser.addActionListener(this);
		 btnadmin.addActionListener(this); 
		  
	    }
	public static void main(String[] args) {
		MainLogin frame=new MainLogin();
		 frame.setTitle("Login");
		     frame.setVisible(true);
	      frame.setBounds(10, 10, 400, 400);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnuser)
		{
			Login frame=new Login(a1);
			 frame.setTitle("Login");
			     frame.setVisible(true);
		      frame.setBounds(10, 10, 400, 400);
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        this.dispose();
		}
		if(e.getSource()==btnadmin)
		{
			AdminLogin frame=new AdminLogin();
			 frame.setTitle("Login");
			     frame.setVisible(true);
		      frame.setBounds(10, 10, 400, 400);
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        this.dispose();
		}
	}

}
