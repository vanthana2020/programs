package com.atm;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;


public class WithdrawFrame extends JFrame implements ActionListener{


	static WithdrawFrame wframe;
	private JTextField txtacno;
	private JTextField txtamt;
	private JTextField txtdate;
	public JButton btnwithdraw;
	public Account a1;
	private JButton btnback;
	private JLabel lblbal;
	public WithdrawFrame(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		JLabel lblDeposit = new JLabel("WITHDRAWAL");
		lblDeposit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblDeposit.setBounds(177, 28, 97, 21);
		getContentPane().add(lblDeposit);
		
		JLabel lblacno = new JLabel("ACCOUNT NUMBER");
		lblacno.setBounds(40, 75, 117, 21);
		getContentPane().add(lblacno);
		
		JLabel lblamt = new JLabel("AMOUNT");
		lblamt.setBounds(40, 119, 97, 21);
		getContentPane().add(lblamt);
		
		JLabel lbldate = new JLabel("DATE");
		lbldate.setBounds(40, 161, 97, 21);
		getContentPane().add(lbldate);
		Integer a=a1.getAcno();
		txtacno = new JTextField();
		txtacno.setBounds(212, 75, 144, 21);
		getContentPane().add(txtacno);
		txtacno.setColumns(10);
		txtacno.setText(a.toString());;
		txtamt = new JTextField();
		txtamt.setBounds(212, 119, 144, 21);
		getContentPane().add(txtamt);
		txtamt.setColumns(10);
		
		txtdate = new JTextField();
		txtdate.setBounds(212, 161, 144, 21);
		getContentPane().add(txtdate);
		txtdate.setColumns(10);
		
		btnwithdraw = new JButton("WITHDRAW");
		btnwithdraw.setBounds(48, 210, 109, 21);
		getContentPane().add(btnwithdraw);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		Date date = new Date();
		txtdate.setText(dateFormat.format(date));
		 Double bal=a1.getBalance();
		 lblbal = new JLabel(bal.toString());
		lblbal.setBounds(384, 75, 46, 14);
		getContentPane().add(lblbal);
		btnback = new JButton("BACK");
		btnback.setBounds(212, 209, 89, 23);
		getContentPane().add(btnback);
		this.a1=a1;
		
		addActionEvent();
	}
	public void addActionEvent() {
		btnwithdraw.addActionListener(this);
		 btnback.addActionListener(this); 
		  
	    }

	public boolean validatefields(int acno,float amt) 
	{
		
		boolean res=false;

		if( amt>=1 && amt<=30000)
			res=true;
		else
		
		     res=false;
		
		
		return res;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
		Integer acno=Integer.parseInt(txtacno.getText());
		Float amt=Float.parseFloat(txtamt.getText());
		
			if(validatefields(acno,amt))
			{
				
			if (e.getSource()== btnwithdraw) 
			 {
				
			 Withdrawchk w=new Withdrawchk(a1);
			 Account a;
				a = w.withdraw(acno,amt);
			
			 if(a!=null)
			 { ImageIcon icon = new ImageIcon("d:/atmicon3.png");
			 JOptionPane.showMessageDialog(null, "Amount withdrawed succesfully\nyour balance is "+a.getBalance(),"Withdraw",JOptionPane.INFORMATION_MESSAGE,icon);
				 
				 MenuFrame mframe=new MenuFrame(a1);
					 mframe.setVisible(true);
				        mframe.setBounds(50, 50, 600,600);
					this.dispose();
				 
			 }
			 else
				 JOptionPane.showMessageDialog(this, "Withdrawal is not successfull"); 
				}
			
			 
			}
			else
				JOptionPane.showMessageDialog(this,"Amount withdhraw should be less than 30000 ");
			
		}
		catch(NumberFormatException e1)
		{

		    //JOptionPane.showMessageDialog(null, "The Server is not accessible or it may be down because of Network Issue", "ERROR", JOptionPane.ERROR_MESSAGE);
		   

		    JOptionPane.showMessageDialog(this,"Withdraw amt should not be empty");
		    //System.out.println(e1);
		}
		if (e.getSource()== btnback) 
		 {
			
		 MenuFrame mframe=new MenuFrame(a1);
			 mframe.setVisible(true);
		        mframe.setBounds(50, 50, 600,600);
			this.dispose();
		 
		 }
	
	}
}

