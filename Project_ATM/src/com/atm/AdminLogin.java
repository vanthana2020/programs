package com.atm;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class AdminLogin extends JFrame implements ActionListener {
	private JTextField txtuname;
	private JPasswordField pwd;
	private JButton btnlogin;
	private JLabel lblNewLabel_2;
	public AdminLogin() {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("User name");
		lblNewLabel.setBounds(52, 68, 109, 22);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setBounds(52, 114, 46, 14);
		getContentPane().add(lblNewLabel_1);
		
		txtuname = new JTextField();
		txtuname.setBounds(223, 69, 86, 20);
		getContentPane().add(txtuname);
		txtuname.setColumns(10);
		
		pwd = new JPasswordField();
		pwd.setBounds(223, 111, 86, 20);
		getContentPane().add(pwd);
		
		 btnlogin = new JButton("Login");
		btnlogin.setBounds(141, 171, 89, 23);
		getContentPane().add(btnlogin);
		
		lblNewLabel_2 = new JLabel("ADMIN LOGIN");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setBounds(143, 23, 114, 14);
		getContentPane().add(lblNewLabel_2);
		addActionEvent();
	}
	public void addActionEvent() {
		 btnlogin.addActionListener(this);
		
		  
	    }
	@Override
	public void actionPerformed(ActionEvent e) {
	 if(e.getSource()==btnlogin)
		 
	 {
		 if(txtuname.getText().equals("admin")&&pwd.getText().equals("admin"))
		 {
			 JOptionPane.showMessageDialog(this,"Login succesfull");
			 AdminPage p=new AdminPage();
			 p.setTitle("Admin page");
			p.setVisible(true);
		    p.setBounds(50, 50, 600,600);
		     this.dispose();
		 }
		 else
			 JOptionPane.showMessageDialog(this,"Enter correct correct username & password");
			
	 }
		
	}
}
