package com.atm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;

public class TransacFrame extends JFrame implements ActionListener {
	static TransacFrame tframe;
	private JTextField txtfacno;
	private JTextField txtamt;
	private JTextField txtdate;
	public JButton btntrans;
	private JTextField txttoaccno;
	public Account a1;
	private JButton btnback;
	private JLabel lblbal;
	private JLabel lblNewLabel;
	public TransacFrame(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		JLabel lblTransfer = new JLabel("TRANSFER MONEY");
		lblTransfer.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		lblTransfer.setBounds(177, 28, 128, 21);
		getContentPane().add(lblTransfer);
		
		JLabel lblfacno = new JLabel("TRANSFER FROM ACCOUNT NO");
		lblfacno.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblfacno.setBounds(40, 94, 177, 21);
		getContentPane().add(lblfacno);
		
		JLabel lblamt = new JLabel("AMOUNT");
		lblamt.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblamt.setBounds(40, 164, 97, 21);
		getContentPane().add(lblamt);
		
		JLabel lbldate = new JLabel("DATE");
		lbldate.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbldate.setBounds(40, 196, 97, 21);
		getContentPane().add(lbldate);
		Integer a=a1.getAcno();
		txtfacno = new JTextField();
		txtfacno.setEditable(false);
		 txtfacno.setText(a.toString());
		txtfacno.setBounds(302, 94, 144, 21);
		getContentPane().add(txtfacno);
		txtfacno.setColumns(10);
		
		txtamt = new JTextField();
		txtamt.setText("");
		txtamt.setBounds(302, 164, 144, 21);
		getContentPane().add(txtamt);
		txtamt.setColumns(10);
		
		txtdate = new JTextField();
		txtdate.setBounds(302, 196, 144, 21);
		getContentPane().add(txtdate);
		txtdate.setColumns(10);
		
		btntrans = new JButton("TRANSFER");
		btntrans.setFont(new Font("Tahoma", Font.BOLD, 11));
		btntrans.setBounds(99, 245, 89, 23);
		getContentPane().add(btntrans);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		Date date = new Date();
		txtdate.setText(dateFormat.format(date));
		Double bal=a1.getBalance();
		 lblbal = new JLabel(bal.toString());
		lblbal.setBounds(384, 75, 46, 14);
		getContentPane().add(lblbal);
		JLabel lbltoaccno = new JLabel("TRANSFER TO ACCOUNT NO");
		lbltoaccno.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbltoaccno.setBounds(40, 132, 150, 21);
		getContentPane().add(lbltoaccno);
		
		txttoaccno = new JTextField();
		txttoaccno.setBounds(302, 126, 144, 21);
		getContentPane().add(txttoaccno);
		txttoaccno.setColumns(10);
		
		btnback = new JButton("BACK");
		btnback.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnback.setBounds(245, 245, 89, 23);
		getContentPane().add(btnback);
		
		lblNewLabel = new JLabel("your balance");
		lblNewLabel.setBounds(369, 50, 77, 21);
		getContentPane().add(lblNewLabel);
		this.a1=a1;
		addActionEvent();
	}
	public void addActionEvent() {
		 btntrans.addActionListener(this);
		 btnback.addActionListener(this);
	   }

	public boolean validatefields(int acno,float amt)
	{
		
		boolean res=false;
		if( amt>=1 && amt<=30000)
			res=true;
		else
			res=false;
		return res;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(txtfacno.getText().equals(txttoaccno.getText()))
		{
			JOptionPane.showMessageDialog(this, "You can;t transfer to yourself");
		}
		try {
		Integer fracno=Integer.parseInt(txtfacno.getText());
		Integer toacno=Integer.parseInt(txttoaccno.getText());
		Float amt=Float.parseFloat(txtamt.getText());
		
			if(validatefields(fracno,amt))
		{
			
		if (e.getSource()== btntrans) 
		 {
			
		 Transferchk t=new Transferchk(a1);
		 Account a;
		 a=t.transfer(fracno,toacno,amt);
		 if(a!=null)
		 {
			 ImageIcon icon = new ImageIcon("d:/atmicon5.png");
			 JOptionPane.showMessageDialog(null, "Amount transfered succesfully","Deposit successfull",JOptionPane.INFORMATION_MESSAGE,icon);
		 }
		 else
			 JOptionPane.showMessageDialog(this, "Amount transfereed not succesfull"); 
		 }
		
			}
		else
			JOptionPane.showMessageDialog(this,"Amount withdhraw should be less than 30000 ");
		}
		catch(NumberFormatException e1)
	{
		JOptionPane.showMessageDialog(this, "you should enter amt ");
	}
		if (e.getSource()== btnback) 
		 {
			
		 MenuFrame mframe=new MenuFrame(a1);
			 mframe.setVisible(true);
		        mframe.setBounds(50, 50, 600,600);
			this.dispose();
		 
		 }
	}
}
