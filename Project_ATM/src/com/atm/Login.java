package com.atm;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JTextField;



import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Color;

public class Login extends JFrame implements ActionListener {
	static Login frame;
	private JPasswordField txtpin;
	JLabel lblNewLabel = new JLabel("ATM INTERFACE");
	JButton btpin = new JButton("LOGIN");
	private JTextField txtdebit;
	JLabel lblvalid;
	public static Account a1;
	public Login(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setForeground(new Color(230, 230, 250));
		
		 
		getContentPane().setLayout(null);
		
		
		lblNewLabel.setFont(new Font("Verdana", Font.BOLD, 13));
		lblNewLabel.setBounds(147, 29, 119, 28);
		getContentPane().add(lblNewLabel);
		
		txtpin = new JPasswordField();
		txtpin.setBounds(169, 135, 193, 28);
		getContentPane().add(txtpin);
		txtpin.setColumns(10);
		btpin.setFont(new Font("Tahoma", Font.BOLD, 11));
		btpin.setForeground(new Color(0, 0, 0));
		
		
		btpin.setBounds(147, 204, 103, 28);
		getContentPane().add(btpin);
		
		JLabel lblenterpin = new JLabel("ENTER PIN");
		lblenterpin.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblenterpin.setBounds(21, 142, 89, 14);
		getContentPane().add(lblenterpin);
		
		JLabel lblenterdebit = new JLabel("ENTER DEBIT CARD NO");
		lblenterdebit.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblenterdebit.setBounds(21, 94, 129, 14);
		getContentPane().add(lblenterdebit);
		
		txtdebit = new JTextField();
		txtdebit.setBounds(169, 87, 193, 28);
		getContentPane().add(txtdebit);
		txtdebit.setColumns(10);
		
		 lblvalid = new JLabel("");
		lblvalid.setForeground(Color.RED);
		lblvalid.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblvalid.setBounds(135, 165, 46, 14);
		getContentPane().add(lblvalid);
		this.a1=a1;
		 addActionEvent();
	}
	public void addActionEvent() {
		 btpin.addActionListener(this);
		 
		  
	    }
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		try{

		Integer pin=Integer.parseInt(txtpin.getText());
		Integer debit=Integer.parseInt(txtdebit.getText());
		System.out.println(pin);
	     System.out.println(debit);
		
			if (e.getSource()== btpin) 
		
		     {
		 Loginchk l=new Loginchk();
		 Account a1=(l.loginck(pin,debit));
		   if(a1!=null)
		     {
			
			 JOptionPane.showMessageDialog(this, "Welcome "+a1.getName()+"\nYour accno"+a1.getAcno()+"\nYour balance "+a1.getBalance());
			 MenuFrame mframe=new MenuFrame(a1);
			 mframe.setVisible(true);
			 mframe.setTitle("Menu Frame");
		      mframe.setBounds(50, 50, 600,600);
			
			 this.dispose();
			 
		     }
		   else
			 JOptionPane.showMessageDialog(this, "check your pin and card no");
		    }
		  } catch (NumberFormatException e1) {
	    
	    	JOptionPane.showMessageDialog(this,"Enter pin and debit card no");
	}
     
	}
	public static void main(String[] args) {
		frame=new Login(a1);
		 frame.setTitle("Login");
		     frame.setVisible(true);
	      frame.setBounds(10, 10, 400, 400);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	     //   frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 	     
	}
	}

