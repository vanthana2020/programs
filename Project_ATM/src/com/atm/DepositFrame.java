package com.atm;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class DepositFrame extends JFrame implements ActionListener{
	static DepositFrame dframe;
	private JTextField txtacno;
	private JTextField txtamt;
	private JTextField txtdate;
	public JButton btndep;
	public JButton btnback;
	private JLabel lblbal;
	public Account a1;
	public DepositFrame(Account a1) {
		getContentPane().setBackground(new Color(176, 196, 222));
		getContentPane().setLayout(null);
		
		JLabel lblDeposit = new JLabel("DEPOSIT");
		lblDeposit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblDeposit.setBounds(177, 28, 97, 21);
		getContentPane().add(lblDeposit);
		
		JLabel lblacno = new JLabel("ACCOUNT NUMBER");
		lblacno.setBounds(40, 75, 117, 21);
		getContentPane().add(lblacno);
		
		JLabel lblamt = new JLabel("AMOUNT");
		lblamt.setBounds(40, 119, 97, 21);
		getContentPane().add(lblamt);
		
		JLabel lbldate = new JLabel("DATE");
		lbldate.setBounds(40, 161, 97, 21);
		getContentPane().add(lbldate);
		Integer a=a1.getAcno();
		txtacno = new JTextField();
		txtacno.setEditable(false);
	    txtacno.setText(a.toString());
		txtacno.setBounds(212, 75, 144, 21);
		getContentPane().add(txtacno);
		txtacno.setColumns(10);
		
		txtamt = new JTextField();
	
		txtamt.setBounds(212, 119, 144, 21);
		getContentPane().add(txtamt);
		txtamt.setColumns(10);
		
		txtdate = new JTextField();
		txtdate.setBounds(212, 161, 144, 21);
		getContentPane().add(txtdate);
		txtdate.setColumns(10);
		
		btndep = new JButton("DEPOSIT");
		btndep.setBounds(75, 210, 89, 23);
		getContentPane().add(btndep);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		Date date = new Date();
		txtdate.setText(dateFormat.format(date));
		
		btnback = new JButton("BACK");
		
		btnback.setBounds(225, 210, 89, 23);
		getContentPane().add(btnback);
	     Double bal=a1.getBalance();
		 lblbal = new JLabel(bal.toString());
		lblbal.setBounds(384, 75, 46, 14);
		getContentPane().add(lblbal);
		
		JLabel lblNewLabel = new JLabel("your balance");
		lblNewLabel.setBounds(384, 51, 77, 21);
		getContentPane().add(lblNewLabel);
		this.a1=a1;
		addActionEvent();
	}
	public void addActionEvent() {
		 btndep.addActionListener(this);
		 btnback.addActionListener(this);
		  
	    }

	

	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			Integer acno=Integer.parseInt(txtacno.getText());
		Float amt=Float.parseFloat(txtamt.getText());
		
		
		if (e.getSource()== btndep) 
		 {
			
		 Depositchk d=new Depositchk();
		 if(d.deposit(acno,amt))
		 {
			 ImageIcon icon = new ImageIcon("d:/atmicon5.png");
			 JOptionPane.showMessageDialog(null, "Amount deposited succesfully","Deposit successfull",JOptionPane.INFORMATION_MESSAGE,icon);
			 MenuFrame mframe=new MenuFrame(a1);
			 mframe.setVisible(true);
		        mframe.setBounds(50, 50, 600,600);
			this.dispose();
		 }
		 else
			 
			 JOptionPane.showMessageDialog(this, "Amount deposited not succesful"); 
			 
		 }
		 
		}catch(NumberFormatException e1)
		{
			JOptionPane.showMessageDialog(this, "you should enter amt ");
		}
		if (e.getSource()== btnback) 
		 {
			//this.dispose();
		     MenuFrame mframe=new MenuFrame(a1);
			 mframe.setVisible(true);
		     mframe.setBounds(50, 50, 600,600);
			this.dispose();
		 
		 }
		
	}
}
