
insert into student values(6,'nandhini',96);
select * from student;
select name,rollno from student where name like 'd%' or name like 'A';
select name,rollno,perc from student where perc>90;
select * from student  order by name desc;
select rpad(name,5,"*") from student;
select max(perc),min(perc) from student;
update student set perc=95 where rollno=1;
delete from student where name like "priya";
alter table student add email varchar(50);
desc student;
alter table student modify column email varchar(90);
alter table student drop perc;
drop table student;
create table student
( rollno int primary key auto_increment,name varchar(50) not null,mobileno int unique,empno int,age int,check(age>0),foreign key(empno) references teacher(empno)
);
insert into teacher values(102,"radha",95000);
insert into student(name,mobileno,age,empno)values("radha",12365,20,102);
create table course(courseid int(4) primary key,coursename varchar(20),duration int,fees float(7,2));
create table student1(studid int(4) primary key,firstname varchar(20),lastname varchar(20),street varchar(20),city varchar(20),DOB date);
create table registration(courseid int(4),studid int(4),DOJ date,foreign key(courseid) references course(courseid),foreign key(studid) references student1(studid));
insert into course values(1001,"Java",4,5000),(1002,"C++",2,4000),(1003,"Linux and C",3,4000),(1004,"Oracle",2,3000);
alter table student1  auto_increment=3001;

insert into student1(
studid,firstname,lastname,street,city,DOB)values(3004,"Priya","Shankar","Gandhipuram","Coimbatore",'1990-01-22');
select * from course;
insert into registration(courseid,studid,DOJ)values(1002,3003,'2011-04-18');

delete from student1 where studid=1000;
alter table course add constraint c1 check(fees>0);
update student1 set age=Year(curdate())-year(DOB);
update course set fees=fees-500 where duration<=3;
delete from student1 where lastname like "Shankar" and city like "Coimbatore";
alter table registration add constraint c1 foreign key 'reg_studid' references 'student1'('studid') on delete cascade;
alter table registration rename column studid to reg_studid;
show create table registration;

'registration', 'CREATE TABLE `registration` (\n  `courseid` int DEFAULT NULL,\n  `reg_studid` int DEFAULT NULL,\n  `DOJ` date DEFAULT NULL,\n  KEY `courseid` (`courseid`),\n  KEY `studid` (`reg_studid`),\n  CONSTRAINT `registration_ibfk_1` FOREIGN KEY (`courseid`) REFERENCES `course` (`courseid`),\n  CONSTRAINT `registration_ibfk_2` FOREIGN KEY (`reg_studid`) REFERENCES `student1` (`studid`)\n) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci'
alter table registration drop foreign key registration_ibfk_2;
select * from student1;
alter table registration add foreign key(reg_studid) references student1(studid) on delete cascade;
select * from course where coursename="C++";
select * from course where fees>4000;
select * from student1 where month(DOB)>=01 and month(DOB)<=09 ;
select * from student1 where age=(select max(age) from student1);
select * from student;
use hr;
select * from employees;
select city from student1 group by city;
desc departments;
