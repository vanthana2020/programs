package filehandlingprgms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FilereadCount {

	public static void main(String[] args) {
		try {
			FileInputStream in=new FileInputStream("d:\\Testdoc.txt");
			int i=0,now=0,nod=0;
			
			while((i=in.read())!=-1)
			{
				  if(Character.isAlphabetic((char)i))
					  now++;
				  else if(Character.isDigit((char)i))
					  nod++;
				  else 
					  continue;
						
			}
			 System.out.println("No of words is "+now);
			 System.out.println("No of digits is "+nod);
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("File doesn't exist");
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			System.out.println("File can't be read");
			e1.printStackTrace();
		}
	

	}

}
