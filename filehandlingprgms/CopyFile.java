package filehandlingprgms;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class CopyFile {

	public static void main(String[] args) {
		 
		try {
			FileInputStream fis = new FileInputStream("d:\\source.txt");
			FileOutputStream fos=new FileOutputStream("d:\\dest.txt");
			int b;
			while((b=fis.read())!=-1)
			{
				fos.write(b);
				
			}
			System.out.println("Copied succesfully");
	        fis.close();
	        fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		     
	}

}
