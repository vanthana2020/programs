package filehandlingprgms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Fileread {

	public static void main(String[] args) {
		try {
			FileInputStream in=new FileInputStream("d:\\Testdoc.txt");
			int i=0;
			while((i=in.read())!=-1)
			{
				System.out.print((char)i);
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("File doesn't exist");
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			System.out.println("File can't be read");
			e1.printStackTrace();
		}
		
	}

}
