package filehandlingprgms;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Filewrite {

	public static void main(String[] args) {
		try {
			FileOutputStream out=new FileOutputStream("d:\\Test");
			out.write(65);
			String str="This is program to test writing contents into a file \n hello world!";
			byte[] arr=str.getBytes();
			out.write(arr);
			System.out.println("Done");
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			System.out.println("File doesn't exist");
			e.printStackTrace();
		} catch (IOException e1) {
			System.out.println("File can't be written");
			e1.printStackTrace();
		}
		

	}

}
