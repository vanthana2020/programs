package filehandlingprgms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class NoofWordsLines {

	public static void main(String[] args){
		try {
			FileInputStream in=new FileInputStream("d:\\Testdoc.txt");
			int i=0,nowords=0,noln=1;
			
			while((i=in.read())!=-1)
			{
				  if(Character.isSpace((char)i))
					  nowords++;
				   if((char)i=='\n')
					  noln++;
				 		
			}
			 System.out.println("No of words is "+nowords);
			 System.out.println("No of lines is "+noln);
			in.close();
		} catch (FileNotFoundException e) {
			System.out.println("File doesn't exist");
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			System.out.println("File can't be read");
			e1.printStackTrace();
		}
	

	}

}
