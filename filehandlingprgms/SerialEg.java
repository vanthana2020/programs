package filehandlingprgms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerialEg {

	public static void main(String[] args) {
		Employee e=new Employee("priya",20000,12);
		try {
			//serialization
			FileOutputStream out = new FileOutputStream("d:\\emp.txt");
			ObjectOutputStream o=new ObjectOutputStream(out);
			o.writeObject(e);
			o.flush();
			o.close();
			out.close();
			System.out.println("done serialization ");
			FileInputStream in=new FileInputStream("d:\\emp.txt");
			ObjectInputStream oi=new ObjectInputStream(in);
			Employee e1=(Employee) oi.readObject();
			System.out.println("done deserialization ");
			System.out.println(e1);
			
			oi.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		

	}

}
