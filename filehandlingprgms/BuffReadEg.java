package filehandlingprgms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class BuffReadEg {

	public static void main(String[] args) {
		
		try {
			File f=new File("d:\\Testdoc.txt");
			FileReader fr= new FileReader(f);
			BufferedReader br=new BufferedReader(fr);
			String s;
			int countln=0,countword=0;
			while((s=br.readLine())!=null)
			{
				System.out.println(s);
				String words[]=s.split(" ");
				{
					countword+=words.length;		
				}
				countln++;
			}
			br.close();
			System.out.println("No of words "+countword);
			System.out.println("No of lines "+countln);
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("File cant be read");
			e.printStackTrace();
		}
		

	}

}
