package filehandlingprgms;

import java.io.Serializable;


public class Employee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3866692518768456105L;
	@Override
	public String toString() {
		return "Employee [name=" + name + ", sal=" + sal + ", empid=" + empid + "]";
	}
	public String name;
	public double sal;
    public int empid;
	public Employee(String name, double sal, int empid) {
		super();
		this.name = name;
		this.sal = sal;
		this.empid = empid;
	}
	
}
