package com.emp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class ArrListEg {

	public static void main(String[] args) {
	     ArrayList<Integer> al = new ArrayList<Integer>();
	     al.add(10);     al.add(20);     al.add(20);     al.add(220);     al.add(30);
	     System.out.println(al);
	     System.out.println(al.get(2));
	     al.set(2, 100);
	     System.out.println(al);
	     al.remove(3);
	     System.out.println(al);
	     System.out.println(al);
	     System.out.println(al.indexOf(30));
	     for(int i = 0 ; i<al.size();i++)
	    	 System.out.println(al.get(i));
	     
	     Iterator<Integer> i = al.listIterator();
	     while(i.hasNext())
	    	 System.out.println(i.next());   
	   LinkedList<Integer> ll = new LinkedList<Integer>();
	     ll.add(200);ll.add(500);ll.add(0, 34);ll.add(67);ll.addFirst(123);
	     System.out.println(ll);

	}

}
