const express = require("express");
const bodyParser = require("body-parser");
 const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true,
})
); 

 app.get("/",(_req,res) => {
     res.send("Node with express is up and running");

 });
 app.listen(3000,()=> 
 {
     console.log("Node with express is up and running and this is console");
 });

const routes = require("./routes/dbroutes");
//const { json } = require("body-parser");

 app.use("/routes",routes);


