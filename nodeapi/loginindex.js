var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var path = require('path');
var app = express();

var session=require("express-session");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(session({
    secret:'secret',
    resave:'true',
    saveUnintialized:'true'
}));
//give the connection settings
var conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "vanthana@123",
    //port
    database: "test"
});

//establish connection 

conn.connect(function(err)
{
    if (err) throw err;
    console.log("Connected to mysql sucessfully");
});


//To show login page 

app.get("/",function(request,response)
{
    response.sendFile(path.join(__dirname + '/home.html')); // remember always to add a relative path 
});
app.get("/login",function(request,response)
{
    response.sendFile(path.join(__dirname + '/login.html')); // remember always to add a relative path 
});
app.get("/reg",function(request,response)
{
    response.sendFile(path.join(__dirname + '/reg.html')); // remember always to add a relative path 
});
app.get("/dashb",function(request,response)
{
    if(request.session.loggedIn) 
    {
      response.write(`<h1>Hello ${request.session.username} </h1>`);  
       response.end("<a href=" + "/logout"+ ">Click here to log out</a>");
      //response.sendFile(path.join(__dirname + '/dashboard.html'));

    }
    else
        response.send("Login first");
  
     
});
app.get("/logout",function(request,response)
{
       //response.send("Welcome "+request.session.username);
          request.session.destroy();
          response.sendFile(path.join(__dirname + '/home.html'));
     
});
app.get("/home",function(request,response)
{
   if(request.session.loggedIn) 
   {
       response.send("Welcome "+request.session.username);
       request.session.destroy();
   }
   else
       response.send("Login first");
      
});
app.post("/auth",function(request,response) // execute when html -submit is pressed 
{
    var username = request.body.username;
    var password = request.body.password;
    console.log(username + "  " + password);
   // check in database
   conn.query("select * from accounts where username = ? and password = ?",[username,password],function(error,result)
   {
  if(error) throw error;
    if(result.length >0)
    {
        console.log("Login sucess");
        request.session.loggedIn=true;
        request.session.username=username;
        response.redirect('/dashb');
        response.end;
    }
        else 
       console.log("Login is not success");
   });
});
app.post("/reguser",function(request,response) // execute when html -submit is pressed 
{
    
  
    var username = request.body.username;
    var password = request.body.password;
    var cpwd=request.body.cpassword;
    var email=request.body.email;
    var city=request.body.city;
    console.log(username + "  " + password);
   // check in database
    // check unique email address
  var sql='SELECT * FROM accounts WHERE  email=?';
  conn.query(sql, [email] ,function (err, data, fields) {
   if(err) throw err
   if(data.length>1){
     
       response.send("user already exists")
   }else if(cpwd != password){
     
      response.send("Password must match")
   }
   else if(!(email.match(pattern))){
    var pattern=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    console.log(pattern.test(email));
    response.send("Enter valid email");
 }else{
   conn.query("insert into accounts (username,password,email,city) values ('"+username+"','"+password+"','"+email+"','"+city+"')",(err,res)=>
   {
       if(err) throw err;
       console.log("record inserted successfully");
       console.log("last inserted",res.insertId);
       request.session.loggedIn=true;
       response.redirect('/login');
       
       response.end;
   }
  
   );
}
});
});
app.listen(4000); 