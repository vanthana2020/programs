import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ])
  ]
})
export class WishlistComponent implements OnInit {
  listItem: string[] = [];
  goal:string;

  currentState = 'initial';

  addElement(): void {
    const listitem: string = this.goal;
   
    this.listItem.push(listitem);
  }
  removeElement(): void {
    this.listItem.length -= 1;
  }
 
  constructor() { }

  ngOnInit(): void {
  }

}
