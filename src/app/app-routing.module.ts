import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimiComponent } from './animi/animi.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegformComponent } from './regform/regform.component';
import { WishlistComponent } from './wishlist/wishlist.component';

const routes: Routes = [
  {path:'animi',component:AnimiComponent},
{path:'',component:HomeComponent},
{path:'login',component:LoginComponent},
{path:'regf',component:RegformComponent},
{path:'gallery',component:GalleryComponent},
{path:'wish',component:WishlistComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
