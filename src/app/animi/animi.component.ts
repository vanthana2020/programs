import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-animi',
  templateUrl: './animi.component.html',
  styleUrls: ['./animi.component.css'],
   animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(10000)),
    ]),
        trigger('changeDivDimension', [
      state('initial', style({
        backgroundColor: 'purple',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'blue',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('150ms')),
      transition('final=>initial', animate('100ms'))
    ]),
  ]
})


export class AnimiComponent {
  title = 'Demo AngularProject'; // variable
  listItem: string[] = [];
  listOrder = 1;

  currentState = 'initial';

  addElement(): void {
    const listitem: string = 'ListItem ' + this.listOrder;
    this.listOrder++;
    this.listItem.push(listitem);
  }
  removeElement(): void {
    this.listItem.length -= 1;
  }


changeState(): void {
  this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
}

}
