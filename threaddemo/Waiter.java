package threaddemo;

public class Waiter implements Runnable {
   private Message msg;
   public Waiter(Message m)
   {
	   this.msg=m;
   }
	@Override
	public void run() {
		String name=Thread.currentThread().getName();
		synchronized(msg)
		{
			System.out.println(name+" Waiting to get notified at time "+System.currentTimeMillis());
			try {
				msg.wait();
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}
			System.out.println(name+" Waiter thread is notified at time "+System.currentTimeMillis());
			System.out.println(name+" processed "+ msg.getMsg());
		}
		
	}

}
