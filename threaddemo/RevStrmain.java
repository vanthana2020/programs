package threaddemo;

public class RevStrmain {
    public synchronized void reverseString(String str) throws InterruptedException
    {
    	char arr[]=str.toCharArray();
		
		for(int i=arr.length-1;i>=0;i--)
		{ 
	       System.out.print(arr[i]);
	       Thread.sleep(1000);
		}
		System.out.println();
    }
	public static void main(String[] args) {
		RevStrmain r=new RevStrmain();
		Strthrd1 t1=new Strthrd1(r);
		t1.start();
		
        Strthrd2 t2=new Strthrd2(r);
        t2.start();
        
	}

}
