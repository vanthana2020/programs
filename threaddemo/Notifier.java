package threaddemo;

public class Notifier implements Runnable {
	 private Message msg;
	   public Notifier(Message m)
	   {
		   this.msg=m;
	   }
	@Override
	public void run() {
		String name=Thread.currentThread().getName();
		System.out.println(name+" Get started");
		
		try {
			Thread.sleep(1000);
			synchronized(msg)
			{
				msg.setMsg(name+" Notifier work done");
				//msg.notify();
				msg.notifyAll();
			}
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
			System.out.println(name+" Waiter thread is notified at time "+System.currentTimeMillis());
			System.out.println(name+" processed "+ msg.getMsg());
		
		
	}

}
