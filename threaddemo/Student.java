package threaddemo;

public class Student {
private int rollno;
private String sname;
private float perc;
public Student() {
	super();
}
public int getRollno() {
	return rollno;
}
public void setRollno(int rollno) {
	this.rollno = rollno;
}
public String getSname() {
	return sname;
}
public void setSname(String sname) {
	this.sname = sname;
}
public float getPerc() {
	return perc;
}
public void setPerc(float perc) {
	this.perc = perc;
}
@Override
public String toString() {
	return "Student [rollno=" + rollno + ", sname=" + sname + ", perc=" + perc + "]";
}

}
