package threaddemo;

public class Thread1 extends Thread {
	public static void main(String[] args) {
		Thread1 t=new Thread1();
		t.setName("Thread 1");
		t.start();
		t.setPriority(MAX_PRIORITY);
		Thread1 t1=new Thread1();
		t1.setName("Thread 2");
		t1.start();
		Thread1 t2=new Thread1();
		t2.setName("Thread 3");
		t2.start();
		t2.setPriority(MIN_PRIORITY);
	}
	
	 public void run()
	    {
	    	try {
	    		String name=currentThread().getName();
	    		System.out.println("Hello from "+name);
	    		sleep(1000);
	    	
	    }catch(InterruptedException e)
	    	{
	    	e.printStackTrace();
	    	}
	    }

}
