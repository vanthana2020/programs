package modifiers;
import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;
public class Test {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      LocalDate dt=LocalDate.now();
      
      Scanner sc=new Scanner(System.in);
      System.out.println("Enter date of birth");
      String dob=sc.next();
      sc.close();
      LocalDate dt1Date=LocalDate.parse(dob);
      Period p=Period.between(dt1Date, dt);
      System.out.println("Your age is "+p);
      
	}

}
