package com.interfaces;

public class Test2 {

	public static void main(String[] args) {
		Teacher t=new Teacher("anu","science");
		t.takeAtt();
		Student s=new Student(t.name, t.subject, "priya", 12);
		System.out.println(s);
		s.doExcercise();
		s.scoreGood();
		
		Student s1=new Student("priya", "maths", "mena", 13);
		System.out.println(s1);
		s1.doExcercise();
		s1.scoreGood();
		
		Student s2=new Student("laksmi", "social", "dana", 14);
		System.out.println(s2);
		s2.doExcercise();
		s2.scoreGood();
		

	}

}
