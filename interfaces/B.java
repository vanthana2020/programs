package com.interfaces;

import java.io.FileNotFoundException;

public class B {

	public static void main(String[] args) {
		A a=new A();
		try {
			a.openFile("abcd");
		} catch (FileNotFoundException e) {
			System.err.println("File name is invalid");
			e.printStackTrace();
		}

	}

}
