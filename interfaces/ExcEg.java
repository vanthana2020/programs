package com.interfaces;

public class ExcEg {

	public static void main(String[] args) {
		int i=50;
		float a;
		try
		{
			System.out.println("Before division");
			 a=i/0;
			System.out.println(a);
			System.out.println("Good job");
		}
		catch(Exception e)
		{
			System.out.println("We cant divide by zero ");
			System.out.println(e.getMessage());
			e.printStackTrace();
		
		}
		finally
		{
		System.out.println("After division");
		}
	}

}
