package com.interfaces;

public class Minivan implements Vehicle {
	String mfrname;
    int yr,price;
	
	@Override
	public void printData() {
		System.out.println(toString());
		
	}

	@Override
	public String toString() {
		return "Minivan [mfrname=" + mfrname + ", yr=" + yr + ", price=" + price + "]";
	}

	public Minivan(String mfrname, int yr, int price) {
		super();
		this.mfrname = mfrname;
		this.yr = yr;
		this.price = price;
	}
  
	
}
