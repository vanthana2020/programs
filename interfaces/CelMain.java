package com.interfaces;

import java.util.Scanner;

public class CelMain {
    public int toConver(int cel) throws NumberConversionException
    {
    	int far=(cel*(9/5))+32;
    	
    	 if(far==0 || far<0)
    		 throw new NumberConversionException("Farhenheit can't be negative or null");
    	 else 
    		 return far;
    }
	public static void main(String[] args) {
		CelMain c=new CelMain();
       Scanner s=new Scanner(System.in);
       System.out.println("Enter celcius");
       int cel=s.nextInt();
       s.close();
       try {
		
		int far=c.toConver(cel);
		System.out.println(far);
	} catch (NumberConversionException e) {
		
		e.printStackTrace();
	}

	}

}
