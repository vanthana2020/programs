package com.interfaces;

import java.util.HashSet;
import java.util.Iterator;

public class HashSEg {

	public static void main(String[] args) {
		HashSet<Object> set=new HashSet<>();
		set.add(Integer.valueOf(10));
		set.add(Float.valueOf(10.0f));
		set.add(Character.valueOf('a'));
		set.add(new String("chennai"));
		set.add(new String("chennai"));
		System.out.println("Set is"+set);
		HashSet<Integer> set1=new HashSet<Integer>();
		set1.add(12);
		set1.add(78);
		set1.add(100);
		for(Integer no:set1)
			System.out.println(no);
		Iterator<Integer> i=set1.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		//set1.clear();
		set1.remove(78);
		System.out.println(set1);
		if(set1.contains(12))
			System.out.println("It has 12");
		else
			System.out.println("It doesn't has 12");
		if(set1.isEmpty())
			System.out.println("Empty");
		else
			System.out.println("Not empty");
		
	}

}
