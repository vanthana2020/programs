package com.interfaces;

public class Student extends Teacher implements Principal,PTTeacher {
   String stname;
   int rollno;
	
	public Student(String name, String subject, String stname, int rollno) {
	super(name, subject);
	this.stname = stname;
	this.rollno = rollno;
}

	@Override
	public String toString() {
		return "Student [stname=" + stname + ", rollno=" + rollno + ", name=" + name + ", subject=" + subject + "]";
	}

	@Override
	public void doExcercise() {
		System.out.println("Doing Excercise");
		
	}

	@Override
	public void scoreGood() {
		System.out.println("Sure I'll");
		
	}

}
