package com.interfaces;

import java.io.FileNotFoundException;

public class A {
public void openFile(String filename) throws FileNotFoundException
{
   if(filename.equals("abc"))
	   System.out.println("file found");
   else
	   throw new FileNotFoundException();
}
}
