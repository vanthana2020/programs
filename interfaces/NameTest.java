package com.interfaces;

import java.util.Scanner;

public class NameTest {
    public boolean isValid(String name) throws InvalidNameException
    {
    
    
			char arr[]=name.toCharArray();
			for(int i=0;i<arr.length;i++)
			{
				char ch=name.charAt(i);
			if(!((ch>='a'&& ch<='z')||(ch>='A' && ch<='Z')))
				throw new InvalidNameException("Name should not contain numbers and Special characters");
							
			}
			 return true;
		}
		
    
	public static void main(String[] args) {
		NameTest n=new NameTest();
		Scanner s=new Scanner(System.in);
		System.out.println("Enter name");
		String name=s.next();
		s.close();
		try {
			n.isValid(name);
			System.out.println("Name is valid");
		} catch (InvalidNameException e) {
		
			e.printStackTrace();
		}
		
	}

}
