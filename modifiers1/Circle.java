package modifiers1;
import java.util.Scanner;
public class Circle {
	private int radius;
	public double calc_area(int rad)
    {
		return Math.round(3.14*radius*radius);
    	
    }
    public double calc_perim(int rad)
    {
    	return Math.round(2*3.14*radius);
    }
	public static void main(String[] args) {
		Circle c=new Circle();
		Scanner s=new Scanner(System.in);
		System.out.println("Enter radius");
	      c.radius=s.nextInt(); 	
	      s.close();
       System.out.println("Area of circle "+c.calc_area(c.radius));
       System.out.println("Perimeter of circle "+c.calc_perim(c.radius));
       
       
	}

}
