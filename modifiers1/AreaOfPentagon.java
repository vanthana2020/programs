package modifiers1;

public class AreaOfPentagon {
	public static double areapent(int n,int s)
	{
		double temp=2*Math.sqrt(n);
		double temp1=n*(n+temp)*Math.pow(s, 2);
		double res=0.25*Math.sqrt(temp1);
		return res;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
         double result=areapent(5,6);
         System.out.println("Area of pentagon is "+result);
	}

}
