package modifiers1;

public class Employee1 {
    int empno,insen;
    float empsal,deduc;
    String ename;
    
     public Employee1(int empno, float empsal, String ename,int insen) {
		//super();
		this.empno = empno;
		this.empsal = empsal;
		this.ename = ename;
		this.insen=insen;
	}
     public Employee1(int empno,float empsal, String ename,int insen,float deduc) {
		super();
		this.empno = empno;
		this.ename = ename;
		this.empsal=empsal;
		this.insen=insen;
		this.deduc=deduc;
	}
	public Employee1(int empno,float empsal,String ename,float deduc) {
		super();
		this.empno = empno;
		this.empsal=empsal;
		this.ename=ename;
		this.deduc=deduc;
	}
	public Employee1() {
		super();
	}
    public String calcTax()
    {
      String res="No values given";
      return res;
    }
    public float calcTax(float empsal,int incen,float deduc)
    {
    	return (empsal+insen-deduc)*0.5f;
    }
    public float calcTax(float empsal,int incen)
    {
    	return(empsal+incen-0)*0.5f;
    }
    public float calcTax(float empsal,float deduc)
    {
    	return(empsal-deduc)*0.5f;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Employee1 e=new Employee1();
      System.out.println(e);
      String res=e.calcTax();
      System.out.println(res);
      Employee1 e1=new Employee1(13,13000.0f,"anu",2000);
      System.out.println(e1);
      System.out.println("TAx is "+e1.calcTax(e1.empsal, e1.insen));
      
      Employee1 e2=new Employee1(14,15000,"dana",5000,3000.0f);
      System.out.println(e2);
      System.out.println("TAx is "+e2.calcTax(e2.empsal, e2.insen,e2.deduc));
      
      Employee1 e3=new Employee1(15,70000,"meena",3000.0f);
      System.out.println(e3);
      System.out.println("TAx is "+e3.calcTax(e3.empsal, e3.deduc));
      
      
	}
	@Override
	public String toString() {
		return "empno=" + empno + ", empsal=" + empsal + ", ename=" + ename ;
	}
	
}
